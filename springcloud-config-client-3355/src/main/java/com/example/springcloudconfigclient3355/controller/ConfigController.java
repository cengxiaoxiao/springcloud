package com.example.springcloudconfigclient3355.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:
 * @author: zbxComputer
 * @time: 2023/7/5 23:23
 */

@RestController
@RequestMapping("/config")
public class ConfigController {

    @Value("${spring.application.name}")
    private String appName;
    @Value("${eureka.instance.instance-id}")
    private String eureka;
    @Value("${server.port}")
    private String port;


    @RequestMapping("/get")
    public String getConfig() {
        return "appName：" + appName + " eureka：" + eureka + " port：" + port;
    }
}
