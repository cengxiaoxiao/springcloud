package com.zbx.springcloud.service;


import com.zbx.springcloud.pojo.Dept;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(value = "DEPT-PROVIDER", fallbackFactory = DeptClientServiceFallBackFactory.class)//value是微服务的名字
public interface DeptClientService {

    @GetMapping("/dept/get/{id}")
        //服务提供者的方法路径
    Dept queryById(@PathVariable("id") Long id);

    @GetMapping("/dept/list")
    List<Dept> queryALL();

    @GetMapping("/dept/add")
    Dept addDept(Dept dept);
}
