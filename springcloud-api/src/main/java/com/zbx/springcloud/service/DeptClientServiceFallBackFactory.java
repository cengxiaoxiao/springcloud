package com.zbx.springcloud.service;

import com.zbx.springcloud.pojo.Dept;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: zbxComputer
 * @time: 2023/6/10 22:47
 */
//服务降级
@Component
public class DeptClientServiceFallBackFactory implements FallbackFactory<DeptClientService> {
    @Override
    public DeptClientService create(Throwable cause) {
        return new DeptClientService() {
            @Override
            public Dept queryById(Long id) {
                return new Dept().setDname("Hystrix服务降级")
                        .setDeptno(id)
                        .setDb_source("这个id=>" + id + "没有对应的信息，客户端提供了降级，这个服务现在已经被关闭");
            }

            @Override
            public List<Dept> queryALL() {
                return new ArrayList<>();
            }

            @Override
            public Dept addDept(Dept dept) {
                return new Dept("Hystrix服务降级").setDb_source("dept是空的，服务被关闭");
            }
        };
    }
}
