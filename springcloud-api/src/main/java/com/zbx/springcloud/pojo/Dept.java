package com.zbx.springcloud.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @description:部门表实体类
 * @author: zbxComputer
 * @time: 2023/5/23 19:25
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)//链式写法
public class Dept implements Serializable {
    private Long deptno;
    private String dname;
    private String db_source;//看看这个数据在哪个数据库

    public Dept(String dname) {
        this.dname = dname;
    }
}
