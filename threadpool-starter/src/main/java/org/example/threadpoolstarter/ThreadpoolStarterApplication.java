package org.example.threadpoolstarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThreadpoolStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThreadpoolStarterApplication.class, args);
    }

}
