package org.example.threadpoolstarter;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.concurrent.ThreadPoolExecutor;

@SpringBootTest
class ThreadpoolStarterApplicationTests {


    @Resource
    private ThreadPoolExecutor MyThreadPool;

    @Test
    void contextLoads() {
        System.out.println(MyThreadPool.getCorePoolSize());
    }

}
