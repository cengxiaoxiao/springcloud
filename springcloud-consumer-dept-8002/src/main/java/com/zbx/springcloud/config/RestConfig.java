package com.zbx.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @description:
 * @author: zbxComputer
 * @time: 2023/5/23 21:17
 */
@Configuration
public class RestConfig { // Configuration - applicationContext.xml
    //配置负载均衡实现RestTemplate
    //IRule 实现类
    //RoundRobinRule 轮询 默认
    // AvailabilityFilterRule 会先过滤掉访问故障的服务器，然后在对剩下的进行轮询访问
    @Bean
    @LoadBalanced//ribbon
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
