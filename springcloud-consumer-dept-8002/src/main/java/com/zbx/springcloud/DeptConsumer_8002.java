package com.zbx.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @description:
 * @author: zbxComputer
 * @time: 2023/5/23 21:28
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(basePackages = {"com.zbx.springcloud"}) //启动Feign 扫描包
@EnableHystrix
public class DeptConsumer_8002 {
    public static void main(String[] args) {
        SpringApplication.run(DeptConsumer_8002.class, args);
    }
}
