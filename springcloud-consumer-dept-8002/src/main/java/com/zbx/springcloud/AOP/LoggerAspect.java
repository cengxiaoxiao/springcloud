package com.zbx.springcloud.AOP;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: zbxComputer
 * @time: 2023/5/27 23:51
 */
@Component
//@Aspect
public class LoggerAspect {


   // @Before("execution(* com.zbx.springcloud.controller.*(..))")
    public void logger(){
        System.out.println("进入com.zbx.springcloud.controller下的方法之前");
    }
}
