package com.zbx.springcloud.service;

import com.zbx.springcloud.dao.DeptDao;
import com.zbx.springcloud.pojo.Dept;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description:
 * @author: zbxComputer
 * @time: 2023/5/23 20:02
 */
@Service
public class DeptServiceImpl implements DeptService {


    @Resource
    private DeptDao deptDao;

    @Override
    public boolean addDept(Dept dept) {
        if (ObjectUtils.isEmpty(dept))
            return false;
        return deptDao.addDept(dept);
    }

    @Override
    public Dept queryById(Long id) {
        return deptDao.queryById(id);
    }

    @Override
    public List<Dept> queryAll() {
        return deptDao.queryAll();
    }
}
