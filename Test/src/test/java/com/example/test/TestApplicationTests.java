package com.example.test;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.concurrent.ThreadPoolExecutor;

@SpringBootTest
class TestApplicationTests {

    @Resource
    private ThreadPoolExecutor MyThreadPool;

    @Resource
    private Student student;

    @Test
    void contextLoads() {
        System.out.println(MyThreadPool.getCorePoolSize());
    }


    @Test
    void test(){

        System.out.println(student.getName());
        System.out.println(student.getCompany());
        System.out.println(student.getGrade());
        System.out.println(student.getMajor());
        System.out.println(student.getScores());
    }

}
